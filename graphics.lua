local x,y

--[[
the standard graphics pipeline which draws a complete scenery
]]--
function graphics_pipeline(batch,xCam,yCam)
	x = xCam
	y = yCam
	local sonarbubblesStencil = love.graphics.newStencil(graphics_stencilFunction)
	graphics_drawBackground()
        love.graphics.setStencil(sonarbubblesStencil)
	graphics_drawEntities(x,y)
	love.graphics.setStencil()
	graphics_drawMap(batch,x,y)
        love.graphics.setInvertedStencil(sonarbubblesStencil)
        graphics_drawShadow()
	love.graphics.setInvertedStencil()
end

--[[
draws the background or 'water'
]]--
function graphics_drawBackground()
	love.graphics.push()
	love.graphics.setColor(130,180,190)
	love.graphics.rectangle("fill",0,0,3200,3200) --TODO
	love.graphics.pop()
end

--[[
draws all entities. not visible entities will be painted over by background
]]--
function graphics_drawEntities(x,y)
	for i,e in ipairs(entities) do
		e:draw(x,y)
	end
end

--[[
draws the map
]]--
function graphics_drawMap(batch,x,y)
	love.graphics.draw(batch,-x,-y)
end

--[[
draws a transparent shadow
]]--
function graphics_drawShadow()
	love.graphics.push()
	love.graphics.setColor(30,30,130,200)
	love.graphics.rectangle("fill",0,0,3200,3200) --TODO
	love.graphics.pop()
end

--[[
draws the controls on top of everything
]]--
function graphics_drawControls()
end

--[[
iterates over all sonarbubbles and adds a circle stencil for each
]]--
function graphics_stencilFunction()
		for i,s in ipairs(sonarbubbles) do
			love.graphics.circle("fill",s.x-x,s.y-y,s.size/2)
		end
end

--[[
draws a console
]]--
function graphics_drawConsole()
end

