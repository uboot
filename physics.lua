local world
local xGravity = 0
local yGravity = -1

--[[
creates a world (love.physics.world) and sets properties and callback functions
]]--
function physics_createWorld()
	love.physics.setMeter(1) -- number of pixels in one meter, set to 1 because of bugs
	world = love.physics.newWorld(xGravity,yGravity,false) -- num: x gravity, num: y gravity, bool: sleep allowed
	world:setCallbacks(beginContact, endContact, preSolve, postSolve)
end

--[[
iterates over all entities and applies forces on them
]]--
function physics_update(dt)
	world:update(dt)
	for i,e in ipairs(entities) do
		if (e.speed~=0) then
			e.body:applyForce(e.speed,0,e.body:getX(),e.body:getY()) 
		end
		e.body:setGravityScale(getWaterDensity(e.body:getY()-e.density))
	end
end

--[[
creates a body,a shape and a fixture (combined)
TODO massData und damping besser machen 
]]--
function physics_createBodyWithShape(xPos,yPos,shape)
	local body = love.physics.newBody(world,xPos,yPos,"dynamic")
	body:setMassData( 100, 100, 2, 0 ) -- num: mass center x, mass center y, mass, inertia
        body:setLinearDamping(2)  
	local shape = love.physics.newChainShape(true,unpack(shape))
	local fixture = love.physics.newFixture(body,shape,0) -- body, shape, density
	return body, shape, fixture
end

--[[ 
creates a body without shape and fixture
]]--
function physics_createBody(xPos,yPos)
	local body = love.physics.newBody(world,xPos,yPos,"dynamic")
	return body
end

--[[
generates walls from a given map-matrix
]]--
function physics_createWalls(map)
	for i = 1,#map.matrix do
		for j = 1,#map.matrix[1] do
			if map.matrix[i][j] == 2 then
				physics_createStaticWall(i*map.realTileSize,j*map.realTileSize,map.realTileSize)
			end
		end
	end

end

--[[
creates static body with rectangle shape (quad -> walls)
returns nothing
]]--
function physics_createStaticWall(xPos,yPos,size)
	local body = love.physics.newBody(world,xPos,yPos,"static")
	local shape = love.physics.newRectangleShape(-(size/2),-(size/2),size,size)
	local fixture = love.physics.newFixture(body,shape,0)
end

--[[
just returns given value, maybe changed sometimes
]]--
function getWaterDensity(depth)
        return depth
end

function beginContact()
end

function endContact()
end
 
function preSolve()
end

function postSolve()
end
