require 'cam'
require 'map'
require 'entities'
require 'console'
require 'physics'
require 'sonar'

entities = {}
console = {}
cameras = {}
maps = {}
sonarbubbles = {}

function love.load()
	game = true --false if a new map is generated TODO used?
	debug = false 
	MAXFPS = 60 -- TODO used?
	map_grid = {}

	-- create map and physical world 
	Map:generateMap() --TODO maybe use a real map instance to avoid global vars like map_grid
	physics_createWorld()
		
	local noOfTiles = 3
	local tilesPath = "gfx/tilebatch.png"
	local realTileSize = 32
	local miniMapWidth = 250
	local miniMapHeight = 150
	local displayBuffer = 1 -- We have to buffer one tile before and behind our viewpoint. TODO used?? kick..
	-- Create map:
	maps[1] = Map:new({matrix=map_grid,noOfTiles=noOfTiles,tilesPath=tilesPath,realTileSize=realTileSize,displayBuffer=displayBuffer})
	physics_createWalls(maps[1])	
	-- create player
	local startcoords = maps[1]:generateSpawnPoints(100,100) --TODO map size in vars
	entities[1] = Ship:new({})
	entities[1]:init(unpack(startcoords))	
	entities[1]:setSpeed(0) --TODO kick, and maybe kick setter to, not necessary
	entities[1].density = entities[1].body:getY() --TODO kick
	-- create sonar bubble that follows the ship
	sonarbubbles[1] = Sonarbubble:new({size=100})
	sonarbubbles[1]:followEntity(1)
	-- Create main camera and mini map camera
	cameras[1] = Cam:new({anchorX=1,anchorY=1,width=love.graphics.getWidth(),height=love.graphics.getHeight(),zoom=1,map=1})
	cameras[1]:followEntity(1)
	cameras[2] = Cam:new({anchorX=1,anchorY=1,width=miniMapWidth,height=miniMapHeight,zoom=10,map=1})
	cameras[2]:followEntity(1)
--	cameras[2]:setPosition(1,1)
	console = Console:new()
end


function love.update(dt)
	physics_update(dt)
	for i,c in ipairs(cameras) do
		c:updatePosition()	
	end
	sonar_update()
--	process_game_input()
end

function love.draw()
	-- iterate over cameras
	for index,cam in ipairs(cameras) do
		cam:getPicture()
	end
	console:draw()
end


function love.keypressed(key, unicode)
	if not console:input(key) then
		Cam:input(key)
	end
	if key == "escape" then
		love.event.quit()
	end
end
