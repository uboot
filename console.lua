Console = {}

function Console:new()
	self.show = false
	self.inputbuffer = ""
	self.prompt = os.date("%H:%M> ")
	self.togglekey = "^"
	self.lines = {}
	for i=1, 13 do
		self.lines[i] = ""
	end
	self.lines[13] = self.prompt
	local t = {}
	setmetatable(t, self)
	self.__index = self
	return t
end

function Console:draw()
	if not self.show then
		return
	end
	love.graphics.setColor(30,30,30)
	love.graphics.polygon("fill", 0, 0, 0, 200, 800, 200, 800, 0)
	love.graphics.setColor(230,230,230)
	for i,v in ipairs(self.lines) do
		love.graphics.print(v, 10, i*15)
	end
end

function Console:input(key)

-- handle opening and closing of console
	if not self.show then
		if key == self.togglekey then
			self.show = true
			return true
		else
			return false
		end
	end

	if key == self.togglekey then
		self.show = false
		return false
	end


--letters are just printed, other keys are special, handle this

--return: new inputline, execute old one, shift lines up
	if key == "return"  or key == "kp_enter" then
		for i=3,13 do
			if self.inputbuffer == "" then
				self.lines[i-1] = self.lines[i]
			else
				self.lines[i-2] = self.lines[i]
			end
		end
		self.lines[12] = self.inputbuffer --TODO nochmal richtig machen
		self:execute(self.inputbuffer)
		self.inputbuffer = ""

		--just no action for these
	elseif key == "tab" or key == "lalt" or key == "ralt" or key == "lctrl" or key == "rctrl" or key == "lshift" or key == "rshift" or key == "mode" or key == "lsuper" or key == "rsuper" or key == "capslock" or key == "menu"  then

	elseif key == "up" then
		--previous command --TODO
	elseif key == "down" then
		--next command
	elseif key == "left" then
		--shift cursor left
	elseif key == "right" then
		--shift cursor right


	else
		--letters, big or small --TODO: convert symbols
		if love.keyboard.isDown("rshift") or love.keyboard.isDown("lshift") then
			key = string.upper(key)
		end
		self.inputbuffer = self.inputbuffer .. key
	end
	self.lines[13] = self.prompt .. self.inputbuffer
	return true
end

function Console:execute(string)
	p = 4
end