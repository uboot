Sonarbubble = {}

--[[ 
Sonarbubble constructor
Use this with parameters x,y,size
]]--
function Sonarbubble:new(o)
	setmetatable(o, self)
	self.__index = self
        return o
end

function Sonarbubble:followEntity(hauntedEntity)
	self.follow = true
	self.hauntedEntity = hauntedEntity
end

function sonar_update()
	for i,s in ipairs(sonarbubbles) do
		if s.follow then
			s.x = entities[s.hauntedEntity].body:getX()
			s.y = entities[s.hauntedEntity].body:getY()
		end		
	end
end
