require 'cam'

function process_game_input()
        if love.keyboard.isDown("up") then
                cameras[1]:move(0,-2)
        end
        if love.keyboard.isDown("down") then
                cameras[1]:move(0,2)
        end
        if love.keyboard.isDown("left") then
                cameras[1]:move(-2,0)
        end
        if love.keyboard.isDown("right") then
                cameras[1]:move(2,0)
        end
		if love.keyboard.isDown( "escape" ) then
				love.event.push( "q" )
		end		
		-- zoom main map in:
		if love.keyboard.isDown( "c" ) then
				cameras[1]:applyZoom(0.01)
		end
		-- zoom main map out:
		if love.keyboard.isDown( "v" ) then
				cameras[1]:applyZoom(-0.01)
		end
		-- zoom mini map in:
		if love.keyboard.isDown( "n" ) then
				cameras[2]:applyZoom(0.02)
		end	
		-- zoom mini map out:
		if love.keyboard.isDown( "m" ) then
				cameras[2]:applyZoom(-0.02)
		end
end
