require 'inheritance'
require 'physics'

entity = {}

--[[
entity object constructor 
properties: xPos, yPos, speed, density
]]--
function entity:new(o)
	setmetatable(o,self)
	self.__index=self
	return o	
end

--[[
Stub 
]]--
function entity:init(xPos,yPos)
end

-- TODO stub
function entity:setSprite(sprite)
end

--TODO stub
function entity:draw(x,y)
end

-- method to set horizontal speed
function entity:setSpeed(speed)
	self.speed = speed
end

-- method to set density, makes entities 'dive' (NOT density of love.physics)
function entity:setDensity(density)
	self.density = density
end

-- TODO or remove
function entity:setPosition(xPos, yPos)
end

-- SHIP:
Ship ={}
--ship object inherits methods from entity
Ship = inherits(entity)

function Ship:init(xPos,yPos)
	local shipShape = {-5,0,0,5,5,0,0,-5} -- the shape of a ship
	self.test = true
	self.body,self.shape,self.fixture = physics_createBodyWithShape(xPos,yPos,shipShape)
end

--[[
draws a ship TODO new..
]]--
function Ship:draw(camX,camY)
	love.graphics.push()
	love.graphics.setColor(255,255,255)
        local sx = self.body:getX()-camX
        local sy = self.body:getY()-camY
        love.graphics.polygon('fill',sx-5,sy,sx,sy+5,sx+5,sy,sx,sy-5)
	love.graphics.pop()
end

-- TORPEDO:
Torpedo = {}
--torpedo object inherits methods from entity
Torpedo = inherits(entity)

