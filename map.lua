Map = {}	
	
--[[ 
Map constructor
Use this with parameters matrix,noOfTiles,tilesPath,realTileSize,displayBuffer
]]--
function Map:new(o)
	setmetatable(o, self)
	self.__index = self
	o.mapHeight = #o.matrix
	o.mapWidth = #o.matrix[1]
	o:loadTiles()
	return o
end

--TODO verschieben nach graphics
function Map:loadTiles()
	local quads = {} 
--[[	
	for i = 0, (self.noOfTiles-1) do
		quads[i+1] = love.graphics.newQuad((((i+1)*self.realTileSize)), 0, self.realTileSize, self.realTileSize, self.realTileSize*self.noOfTiles, self.realTileSize)
	end
]]--

	quads[1] = love.graphics.newQuad(1,1,32,32,102,34)
	quads[2] = love.graphics.newQuad(35,1,32,32,102,34)
	quads[3] = love.graphics.newQuad(69,32,32,32,102,34)

	local image = love.graphics.newImage( self.tilesPath )
	image:setFilter("nearest","linear")
	image:setWrap("repeat","repeat")
	self.spriteBatch = love.graphics.newSpriteBatch(image, 10000 )
	for x = 1, self.mapWidth do
		for y = 1, self.mapHeight do
			self.spriteBatch:addq(quads[(self.matrix[x][y])+1],((x-1)*self.realTileSize),((y-1)*self.realTileSize),0,1,1,0,0)			
		end
	end
end

--[[ 
it's necessary to recalculate the tile sizes for each cam
TODO: really used?? check this
]]--
function Map:adjustMapProperties(camWidth,camHeight,zoom)
	self.tileSize = math.floor(self.realTileSize/zoom) 
	self.displayWidth = math.ceil(camWidth/self.tileSize)
	self.displayHeight = math.ceil(camHeight/self.tileSize)
end

--map
-- x*y points as base
--the walls are lines between these points
--a line can be straight top-down ( | )  or left-right ( - ), 90° ( 0,2:1,1 / ) or 45° ( 0,0:2,1 )

--map generation algorithm
function Map:generateAdjacentFields(width,height)
	local startx = math.random(width)
	local starty = math.random(height)

	--helper for watching mapsize
	local room_count = 1

	--set up the map grid 100*100 in size
	for i = 1,width do
		map_grid[i] = {}
		for j=1,height do
			map_grid[i][j] = 1 --set every cell of the map to wall
		end
	end

	--mark start point as room ( 0 )
	map_grid[startx][starty] = 0

	--start borders

	map_grid[startx-1][starty] = 2
	map_grid[startx][starty-1] = 2
	map_grid[startx][starty+1] = 2
	map_grid[startx+1][starty] = 2


--active cells are acted on to continue the path
	local active_cells = {{startx,starty}}

	while true do
		cell_set = #active_cells
		for i = 1,cell_set do

--representation of the adaject cells
-- 1 2 3
-- 4 x 5
-- 6 7 8
			current_cell = { active_cells[i][1],active_cells[i][2] } --select x and y coords



			for j = 1,8 do --mark random adjacent cells as active
				if math.random(30) < 12 then
					if j == 1 or j ==  2 or  j == 3 then
						temp_x = -1
					elseif j == 4 or j == 5 then
						temp_x = 0
					else
						temp_x = 1
					end

					if j == 1 or j == 4 or j == 6 then
						temp_y = -1
					elseif j == 2 or j == 7 then
						temp_y = 0
					else
						temp_y = 1
					end
					--mark cell as active and room if not already a room cell and if not out ouf map borders
					new_x = current_cell[1]+temp_x --x coord
					new_y = current_cell[2]+temp_y --y coord
					if (new_x < 98 and new_x > 1 and new_y < 98 and new_y > 1) then --obey map borders
						if (map_grid[ new_x ][ new_y ] == 2 or map_grid[ new_x ][ new_y ] == 1) then --only do something if this cell is not already a room
							table.insert(active_cells, {new_x,new_y} ) --insert to the active cell list
							map_grid[new_x][new_y] = 0 --mark active cell as room

							for k = 1,4 do --mark borders
								if k == 1 then
									temp_x2 = -1
								elseif k == 2 or k == 3 then
									temp_x2 = 0
								else
									temp_x2 = 1
								end

								if k == 2 then
									temp_y2 = -1
								elseif k == 1 or k == 4 then
									temp_y2 = 0
								else
									temp_y2 = 1
								end

								border_x = new_x+temp_x2 --x coord
								border_y = new_y+temp_y2 --y coord

								if map_grid[ border_x ][ border_y ] == 1 then
									map_grid[ border_x ][ border_y ] = 2 --mark as border if its still wall
								end
							end --for k = 1,4 do

							room_count = room_count+1
						end
					end
				end
			end
		end

		for i = 1,cell_set do
			table.remove(active_cells,i)
		end
		if cell_set == 0 then
			break
		end
	end

	print(room_count)
	if room_count < 1500 then
		return false
	else
		return true
	end

end


--TODO kick this function?? is not used
function generate_borders()
	ii = 100
	jj = 100
	for ii = 1,ii do
		for jj = 1,jj do
			if map_grid[ii][jj] == 2 then
				select_tile(ii,jj)
			end
		end
	end
end

--TODO:
-- there are some possibilities for selecting the tile for the border depending on the sorrounding

-- again here:
-- 1 2 3
-- 4 x 5
-- 6 7 8
-- these blocks can be wall, border or space, so there are 3^8 combinations

-- orientation of border
-- bottom	10
-- top	11
-- left	12
-- right	13
-- 45° /	14
-- 45° \	15
-- 22,5° / 1. part	16
-- 22,5° / 2. part	17
-- 22,5° \ 1. part	18
-- 22,5° \ 2. part	19
-- 22,5° bottom-left,top-right 1. part	20
-- 22,5° bottom-left,top-right 2. part	21
-- 22,5* top-left, bottom-right 1. part	22
-- 22,5* top-left, bottom-right 2. part	23

--function select_tile(x,y)
--	if 
--end


--[[
Generates spawn points in the water
]]--
function Map:generateSpawnPoints(width,height)
        repeat
	        randx = math.random(width)
                randy = math.random(height)
        until map_grid[randx][randy] == 0
	startcoords = {randx*self.realTileSize,randy*self.realTileSize}
	return startcoords
end

function Map:generateMap()
	seed = os.time()
	math.randomseed( seed ) --pseudo-random, good enough for us

	while Map:generateAdjacentFields(100,100) == false do
		print "fail"
	end
	print("map #",seed)
	--generate_borders()
end
