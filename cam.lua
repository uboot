require 'map'
require 'physics'
require 'graphics'

Cam = {}

--[[ 
Camera constructor
Use this with parameters x,y,anchorX,anchorY,width,height,zoom,map
]]--
function Cam:new(o)
	setmetatable(o, self)
	self.__index = self
	o.body = physics_createBody()
	return o
end

--[[ 
Set camera to shoot specific map
]]--
function Cam:bindToMap(map)
	self.map = map
end

--[[
Set position of camera's body
]]--
function Cam:setPosition(x,y)
	self.body:setX(x)
	self.body:setY(y)
end

--[[ 
Move camera. Change upper left position by x and y
]]--
function Cam:move(x,y)
	self.body:setPosition(self.body:getX() + x,self.body:getY() + y)
end

--[[ 
Set camera position on screen
]]--
function Cam:setScreenPos(anchorX,anchorY)
	self.anchorX = anchorX
	self.anchorY = anchorY
end

--[[ 
Zoom in or out
TODO calculations are ugly... camera view should be around it's body pos, and not in top left corner
]]--
function Cam:applyZoom(amount)
	local oldwidth = self.width*self.zoom
	local oldheight = self.height*self.zoom
	self.zoom = self.zoom + amount
	local widthDifference = (self.width*self.zoom) - oldwidth
	local heightDifference = (self.height*self.zoom) - oldheight
	self:setPosition(self.body:getX()-(widthDifference/2), self.body:getY()-(heightDifference/2))
end

--[[ 
Change size of camera view, upper left position won't get modified
]]--
function Cam:alterSize(width, height)
	self.width = width
	self.height = height

end

--[[ 
Draw view of camera to screen
]]--
function Cam:getPicture()
	love.graphics.push()
	love.graphics.setScissor(self.anchorX,self.anchorY,self.width,self.height)
	love.graphics.scale((1/self.zoom),(1/self.zoom))  
	graphics_pipeline(maps[self.map].spriteBatch,self.body:getX(),self.body:getY())
	love.graphics.setScissor()
	love.graphics.pop()

end

--[[
Cam will follow specific entity (must be in the entities table), this entity will always be centered
]]--
function Cam:followEntity(entityIndex)
        self.follow = true
        self.hauntedEntity = entityIndex
        -- center entity in middle of camera view:
  self.body:setPosition(entities[self.hauntedEntity].body:getX()+self.anchorX-((self.width*self.zoom)/2),entities[self.hauntedEntity].body:getY()+self.anchorY-((self.height*self.zoom)/2))
  --self.body:setPosition(entities[self.hauntedEntity].body:getX()-((self.width)/2),entities[self.hauntedEntity].body:getY()-((self.height)/2))
end

--[[
Stop following an entity and hold current position
]]--
function Cam:stopFollowing()
        self.follow = false
end

--[[
If cam is set to follow an entity, call this method to check if cam has to move
TODO not sure if zoom calculations are correct..
]]--
function Cam:updatePosition()
        if self.follow then
                local speed_entity_x, speed_entity_y = entities[self.hauntedEntity].body:getLinearVelocity()
                local speed_cam_x, speed_cam_y  = self.body:getLinearVelocity()
                if (entities[self.hauntedEntity].body:getX() > (self.body:getX() + ((self.width*self.zoom)/2) + 20)) or
                        (entities[self.hauntedEntity].body:getX() < (self.body:getX() + ((self.width*self.zoom)/2) - 20)) then
                                self.body:setLinearVelocity(speed_entity_x, speed_cam_y)
                end
                if (entities[self.hauntedEntity].body:getX() == (self.body:getX() + ((self.width*self.zoom)/2))) then
                        self.body:setLinearVelocity(0, speed_cam_y)
                end
                local speed_cam_x, speed_cam_y  = self.body:getLinearVelocity()
                if (entities[self.hauntedEntity].body:getY() > (self.body:getY() + ((self.height*self.zoom)/2) + 20)) or
                        (entities[self.hauntedEntity].body:getY() < (self.body:getY() + ((self.height*self.zoom)/2) - 20)) then
                                self.body:setLinearVelocity(speed_cam_x, speed_entity_y)
                end
                if (entities[self.hauntedEntity].body:getY() == (self.body:getY() + ((self.height*self.zoom)/2))) then
                        self.body:setLinearVelocity(speed_cam_x, 0)
                end
        end
end

function Cam:input(key)
	if key == "c" then
		cameras[1]:applyZoom(0.01)
	elseif key ==  "v" then
		cameras[1]:applyZoom(-0.01)

	-- zoom mini map in:
	elseif key == "n" then
		cameras[2]:applyZoom(0.02)
		-- zoom mini map out:
	elseif  key ==  "m" then
		cameras[2]:applyZoom(-0.02)

	--move map
	elseif key == "up" then
		entities[1].density = entities[1].density - 10  
	elseif key == "down" then
		entities[1].density = entities[1].density + 10  
	elseif key == "left" then
		entities[1].speed = entities[1].speed - 10  
	elseif key == "right" then
		entities[1].speed = entities[1].speed + 10  
	end
end

