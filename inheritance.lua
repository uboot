--[[
this function is used to implement inheritance
]]--
function inherits(baseClass)

        local new_class = {}
        local class_mt = { __index = new_class }

        function new_class:new(o)
                setmetatable(o,class_mt)
                return o
        end 

        setmetatable(new_class,{ __index = baseClass })

        return new_class
end


